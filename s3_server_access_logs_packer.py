#! /usr/bin/env python3
# pylint: disable=line-too-long

"""
Script downloads all your tonns of tiny "S3 Server access logging" files in parallel and gzip them together.
It uses s3 inventory to list files, to not create a lots of requests to s3 (because it cost money and it is slow).
Checking data consistency: script compares that the size in the S3 inventory CSV must be equal with the size of downloaded files and must be equal to the size in the final gzip archive.
Some parts implemented by shell commands: `pigz`, `wc`.

Some links:
* https://docs.aws.amazon.com/AmazonS3/latest/dev/ServerLogs.html
* https://docs.aws.amazon.com/AmazonS3/latest/dev/storage-inventory.html
* https://forums.aws.amazon.com/thread.jspa?threadID=320343

Some statistic example:
t3a.micro instance (1GB RAM, 2vCPU), 100GB SSD EBS, ext4, Ubuntu 20.04, --parallelism 10: can download ~50000 files (this is logs for one day) in 7 minutes and pack them in ~1 minute from 3GB to 340MB gzip (ratio ~9). RAM usage is about 500MB, CPU is about 130%.

Requirements:
* Python 3.7 or newer
* pigz — multithread gzip
* AWS access to S3, one of:
** IAM role (or "instance profile"), this is prefferable method, to avoid secret keys sharing
** or keys in `~/.aws/credentials` and env variable AWS_PROFILE
** or env variables AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY

TODO:
* dryrun (partially)
* exit in case of 100 errors, do not do thousands requests
* find latest s3 inventory manifest
* repeat download if fail
* statistic with timing
* upload results to s3

License: MIT
Link to project: https://gitlab.com/vazhnov/s3-server-access-logs-packer/
"""

import os
import time
import datetime
import json
from shutil import which
import csv
import argparse
import logging
import boto3
from botocore.exceptions import ClientError
from urllib.parse import urlparse
import subprocess
from multiprocessing import Pool


# I'm not sure it should be global, but maybe it is better because of multiprocessing.
s3 = boto3.resource('s3')


def main():
    config = parseconfig()
    if check_command_not_found(['wc', 'pigz']):
        return(1)
    # Download s3 inventory
    s3inventory_path = urlparse(config.s3inventory_manifest)
    INVENTORY_LOCAL_COPY = os.path.join(config.tmpdir, 'inventory', s3inventory_path.netloc)
    os.makedirs(INVENTORY_LOCAL_COPY, exist_ok=True)
    download_s3_inventory(config, s3inventory_path.netloc, s3inventory_path.path, INVENTORY_LOCAL_COPY)

    # date_start = datetime.datetime(2020, 4, 26)
    statistic_files_number = 0
    statistic_files_size = 0
    statistic_files_size_gzip = 0
    for d in daterange_from_number(config.startdate, config.numdays):
        # Preparations
        print('### Day', d)
        LOGS_LOCAL_COPY = os.path.join(config.tmpdir, 'raw_logs', config.s3bucket, d)
        CSVFILE = os.path.join(config.tmpdir, 'inventory', '{}_{}_files_list_from_inventory.csv'.format(config.s3bucket, d))
        ALLLOGSGZIPFILE = os.path.join(config.tmpdir, '{}_{}_s3accesslogs_pack.gz'.format(config.s3bucket, d))
        os.makedirs(LOGS_LOCAL_COPY, exist_ok=True)
        # Run access logs download
        inventory_lines_number = zgrep_inventory(config, d, INVENTORY_LOCAL_COPY, CSVFILE)
        print('# inventory_lines_number:', inventory_lines_number)
        (total_size_from_cvs, files_number) = download_all_s3_from_csv(config, CSVFILE, LOGS_LOCAL_COPY)
        print('# total_size_from_cvs:', total_size_from_cvs, 'files_number:', files_number)
        # Gzip them all
        (total_size_raw, total_size_into_gzip, size_gzip) = pigz_them_all(LOGS_LOCAL_COPY, ALLLOGSGZIPFILE)
        print('# total_size_raw:', total_size_raw, 'total_size_into_gzip:', total_size_into_gzip, 'size gzip:', size_gzip)
        if total_size_raw == total_size_into_gzip and total_size_from_cvs == total_size_raw:
            print('# OK, all sizes are equal!')
            statistic_files_number += files_number
            statistic_files_size += total_size_from_cvs
            statistic_files_size_gzip += size_gzip
            upload_results()
        else:
            print('# Error, something goes wrong, sizes mismatch: {}, {}, {}'.format(total_size_from_cvs, total_size_raw, total_size_into_gzip))
    print('## In total: files number:', statistic_files_number, 'files size:', statistic_files_size, 'files size in gzip:', statistic_files_size_gzip)
    if statistic_files_size_gzip > 0:
        print('## Compression ratio is:', statistic_files_size / statistic_files_size_gzip)
    else:
        print('## Cant calculate compression ratio')


def parseconfig():
    parser = argparse.ArgumentParser(description=__doc__, formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('-n', '--dryrun', default=False, action="store_true", help='Do not do real download, except manifest.json')
    parser.add_argument('--loglevel', default="INFO", choices=['DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL'], help='Set logging level: DEBUG, INFO, WARNING, ERROR, CRITICAL')
    parser.add_argument('--tmpdir', default='/tmp/s3_logs_pack', help='Directory to download all files and create .gz archives')
    parser.add_argument('--s3bucket', required=True, help='S3 bucket which access logs download')
    parser.add_argument('--s3bucket_dst_logs', required=True, help='S3 bucket where logs are stored')
    parser.add_argument('--s3inventory_manifest', metavar='s3://path', required=True, help='S3 path to S3 inventory manifest file')
    parser.add_argument('--parallelism', metavar='INT', default=10, type=int, help='Number of process to run parallel download')
    parser.add_argument('--startdate', metavar='YYYY-MM-DD', required=True, type=datetime.date.fromisoformat, help='The logs start date, can not be newer than current S3 inventory')
    parser.add_argument('--numdays', metavar='INT', default=1, type=int, help='Number of days (of logs) to download')
    args = parser.parse_args()
    logging.basicConfig(level=getattr(logging, args.loglevel))
    for arg, value in sorted(vars(args).items()):
        logging.debug('Argument {}: {}'.format(arg, value))
    return args


def daterange_from_number(start_date, count):
    for n in range(count):
        new_date = start_date + datetime.timedelta(n)
        yield new_date.strftime("%Y-%m-%d")


# def check_command_not_found(binaries: list):
def check_command_not_found(binaries):
    for programm in binaries:
        if which(programm) is None:
            logging.critical('Error: programm is not available in PATH (command not found): {}'.format(programm))
            return True
    return False


# def download_s3_file(s3bucket, s3key, dstdir, expected_size: int):
def download_s3_file(config, s3bucket, s3key, dstdir, expected_size):
    # TODO: repeat if fail
    dstfile = os.path.join(dstdir, os.path.basename(s3key))
    logging.debug('Starting download: "s3://{}/{}" in "{}"'.format(s3bucket, s3key, dstfile))
    if os.path.exists(dstfile):
        if os.path.getsize(dstfile) != expected_size:
            logging.warning('File "{}" exist, but file size mismatch, downloading again'.format(dstfile))
        else:
            logging.debug('OK, already downloaded, size as expected: {}'.format(dstfile))
            return('OK')
    else:
        if config.dryrun:
            time.sleep(0.1)
        else:
            try:
                s3.Bucket(s3bucket).download_file(s3key, dstfile)
            except ClientError as e:
                msg = 'Error: exception when downloading "s3://{}/{}" in "{}": "{}"'.format(s3bucket, s3key, dstfile, e)
                logging.error(msg)
                return(msg)
        return('OK, download finished: s3://{}/{}'.format(s3bucket, s3key))


def download_s3_inventory(config, s3bucket, s3key, dstdir):
    """Parse manifest.json and download inventory files in parallel."""
    logging.info('Download S3 inventory: s3://{}/{} into {}'.format(s3bucket, s3key, dstdir))
    manifest = json.load(s3.Object(s3bucket, s3key.lstrip('/')).get()['Body'])
    arguments_to_submit = list()
    for obj in manifest['files']:
        arguments_to_submit.append((config, s3bucket, obj['key'], dstdir, int(obj['size'])))
    with Pool(config.parallelism) as p:
        result = p.starmap(download_s3_file, arguments_to_submit)
    for r in result:
        if not r.startswith('OK'):
            logging.error('Result is not OK:', r)


def download_all_s3_from_csv(config, CSVFILE, LOGS_LOCAL_COPY):
    """Here we expect to see only one S3 inventory format: bucket, key, size, anything else"""
    logging.info('Download all files from CSV: {} into {}'.format(CSVFILE, LOGS_LOCAL_COPY))
    total_size = 0
    files_number = 0
    arguments_to_submit = list()
    with open(CSVFILE, newline='') as f:
        reader = csv.reader(f)
        for row in reader:
            (s3bucket, s3key, s3size, s3mtime) = row
            total_size += int(s3size)
            # dstfile = os.path.join(LOGS_LOCAL_COPY, os.path.basename(s3key))
            # arguments_to_submit.append((s3bucket, s3key, dstfile, int(s3size)))
            arguments_to_submit.append((config, s3bucket, s3key, LOGS_LOCAL_COPY, int(s3size)))
            files_number += 1
    with Pool(config.parallelism) as p:
        result = p.starmap(download_s3_file, arguments_to_submit)
    for r in result:
        if not r.startswith('OK'):
            logging.error('Result is not OK:', r)
    logging.info('Finished download all files from CSV: {} into {}'.format(CSVFILE, LOGS_LOCAL_COPY))
    return(total_size, files_number)


def zgrep_inventory(config, d, INVENTORY_LOCAL_COPY, dstfile):
    """TODO: Because of pipes in shell commands, we can lose some errors here!"""
    string_to_find = '^"{}","{}/{}{}'.format(config.s3bucket_dst_logs, config.s3bucket, config.s3bucket, d)
    logging.info('Started to zgrep {} in "{}"'.format(string_to_find, INVENTORY_LOCAL_COPY))
    # If previous time was fail, then .csv can be empty
    if not os.path.exists(dstfile) or os.path.getsize(dstfile) == 0:
        subprocess.check_output("zgrep --no-filename '{}' * > {}".format(string_to_find, dstfile), shell=True, cwd=INVENTORY_LOCAL_COPY)
    logging.info('Gzip file "{}"'.format(dstfile))
    # If previous time was fail, then .csv.gz can be older than .csv
    if not os.path.exists('{}.gz'.format(dstfile)) or (os.path.getmtime(dstfile) > os.path.getmtime('{}.gz'.format(dstfile))):
        subprocess.check_output("pigz --best --to-stdout -- {} > {}.gz".format(dstfile, dstfile), shell=True)
    logging.info('Calculate lines in file "{}"'.format(dstfile))
    lines_number = int(subprocess.check_output(['wc', '-l', dstfile]).decode("utf-8").lstrip().split(" ")[0])
    return(lines_number)


def pigz_them_all(LOGS_LOCAL_COPY, dstfile):
    """pigz is like gzip, but use of multiple processors and cores.
    TODO: Because of pipes in shell commands, we can lose some errors here!
    """
    logging.info('Run pigz to all logs in "{}"'.format(LOGS_LOCAL_COPY))
    if not os.path.exists(dstfile) or os.path.getsize(dstfile) == 0:
        # Tricks with `find` and `xargs` to avoid error: /usr/bin/cat: Argument list too long
        subprocess.check_output('find . -maxdepth 1 -type f -print0 | sort -zV | xargs -0 cat | pigz --best --to-stdout - > "{}"'.format(dstfile), shell=True, cwd=LOGS_LOCAL_COPY)
    logging.info('Calculate bytes in all logs from "{}"'.format(LOGS_LOCAL_COPY))
    total_size_raw = int(subprocess.check_output('find . -maxdepth 1 -type f -exec cat {} + | wc --bytes', shell=True, cwd=LOGS_LOCAL_COPY).decode("utf-8").lstrip().split(" ")[0])
    logging.info('total_size_raw: {}'.format(total_size_raw))
    logging.info('Calculate bytes inside final archive "{}"'.format(dstfile))
    total_size_into_gzip = int(subprocess.check_output('zcat {} | wc --bytes'.format(dstfile), shell=True).decode("utf-8").lstrip().split(" ")[0])
    logging.info('total_size_into_gzip: {}'.format(total_size_into_gzip))
    logging.info('Calculate bytes of final archive "{}"'.format(dstfile))
    # TODO: TypeError: bufsize must be an integer
    size_gzip = int(subprocess.check_output('wc --bytes {}'.format(dstfile), shell=True).decode("utf-8").lstrip().split(" ")[0])
    logging.info('size_gzip: {}'.format(size_gzip))
    return(total_size_raw, total_size_into_gzip, size_gzip)


def upload_results():
    pass


if __name__ == "__main__":
    main()
