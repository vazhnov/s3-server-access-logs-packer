# s3\_server\_access\_logs\_packer.py

Script downloads all your tonns of tiny "S3 Server access logging" files in parallel and gzip them together.

It uses s3 inventory to list files, to not create a lots of requests to s3 (because it cost money and it is slow).

Checking data consistency: script compares that the size in the S3 inventory CSV must be equal with the size of downloaded files and must be equal to the size in the final gzip archive.

Some parts implemented by shell commands: `pigz`, `wc`.

## Some statistic example

t3a.micro instance (1GB RAM, 2vCPU), 100GB SSD EBS, ext4, Ubuntu 20.04, --parallelism 10: can download ~50000 files (this is logs for one day) in 7 minutes and pack them in ~1 minute from 3GB to 340MB gzip (ratio ~9). RAM usage is about 500MB, CPU is about 130%.

## Requirements

* Python 3.7 or newer
* [pigz](https://zlib.net/pigz/) — multithread gzip
* AWS access to S3, one of:
  * IAM role (or "instance profile"), this is prefferable method, to avoid secret keys sharing
  * or keys in `~/.aws/credentials` and env variable `AWS_PROFILE`
  * or env variables `AWS_ACCESS_KEY_ID` and `AWS_SECRET_ACCESS_KEY`

## TODO

* dryrun (partially)
* exit in case of 100 errors, do not do thousands requests
* find latest s3 inventory manifest
* repeat download if fail
* statistic with timing
* upload results to s3

## Some connected links

* https://docs.aws.amazon.com/AmazonS3/latest/dev/ServerLogs.html
* https://docs.aws.amazon.com/AmazonS3/latest/dev/storage-inventory.html
* https://forums.aws.amazon.com/thread.jspa?threadID=320343

## License

MIT

## Sources

This project: https://gitlab.com/vazhnov/s3-server-access-logs-packer
